!/usr/bin/php -q
<?php
// Google Auth
//require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');
require_once ('calendar.php');			// caledar generator
require_once ('showCalendar.class.php');	// load showCalendar class

// start recording 
ob_start();
// head and header
echo "<?php\n";
?>
// Google Auth
//require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');
require_once ('calendar.php');			// caledar generator
require_once ('showCalendar.class.php');	// load showCalendar class
require_once ('calendar_head.php');		// basic functions

//$client = new Google_Client();
//$client->setAuthConfig('/home/dnapaint/etc/calendar/client_secret.json');
//$client->addScope(Google_Service_Calendar::CALENDAR);
//$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);

// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;

}
$page_title = "Calendar for  ";		// set page title
include('calendar_header.php');		// html header
<?php
echo "\n?>\n";
// end of header
?>
<body style="margin:0px; height:100%; width:100%">
<?php
// menu
echo "<?php\n";
echo "include('calendar_menu.php');";
echo "echo showMsg();";
echo "\n?>";
// main div start
?>
<div class='container' style='margin:0px auto; padding:0px; width:100%'>
<?php
if(isset($_GET['show-week'])){
	$show_calendar = new showCalendar();
	echo $show_calendar->showWeek();
	// main div end
	?>
	</div>
	<!-- tooltip handler -->
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
	</html>
	<?php
	$feed_data = ob_get_contents();
	// close buffer and create file
	ob_end_clean();
	$fp = fopen("this-week.php", "w");
	fwrite($fp, $feed_data);
	fclose($fp);
}
elseif(isset($_GET['show-day'])){
	$show_calendar = new showCalendar();
	echo $show_calendar->showDay();
	// main div end
	?>
	</div>
	<!-- tooltip handler -->
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
	</html>
	<?php
	$feed_data = ob_get_contents();
	// close buffer and create file
	ob_end_clean();
	$fp = fopen("today.php", "w");
	fwrite($fp, $feed_data);
	fclose($fp);
}
else{
	$calendar = new Calendar();
	echo $calendar->show();
	// main div end
	?>
	</div>
	<!-- tooltip handler -->
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
	</html>
	<?php
	$feed_data = ob_get_contents();
	// close buffer and create file
	ob_end_clean();
	$fp = fopen("index.php", "w");
	fwrite($fp, $feed_data);
	fclose($fp);
}
?>