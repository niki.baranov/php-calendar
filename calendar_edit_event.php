<?php
require_once('calendar_head.php');			// general functions
require_once("db/db-init.php");		// db connection

$page_title = "Edit event";		// set page title
include('calendar_header.php');			// html header

function getEvents($db) {
// get date from url
$event_id = isset($_POST['id']) ? $_POST['id'] : '';
$event_id_ggl	= (isset($_REQUEST['event_id_ggl']))		? $_REQUEST['event_id_ggl'] : '';

// process sql, use prepared statement
$q_events = <<<getEvents
SELECT * 
FROM events 
WHERE event_id=:event_id 
getEvents;
	$events = $db->prepare("$q_events");
	// define parameters in the sql statement
	$events->execute(array(':event_id'=>$event_id));
   return $events; 
}

$events = getEvents($db);
if ($events->rowCount()<=0){
	$_SESSION['fail'] = "No events for today!";
}
else{
	$event = $events->fetch(PDO::FETCH_ASSOC);
}

?>
<body>
<?php
include('calendar_menu.php');	// print top navigation
// page content starts below
?>
<div class="container" style="margin:10px auto">	
<?php
showMsg();

?>
<?php echo "end time".date_create($event['end'])->format("Y-m-d\TH:i"); ?>

<form method='post' action='calendar_edit.php' id='new_event'>
	<table border='0' style='width:100%;'>
		<tr valign='top'>
		  <td align='right'>Contact *
		  <input type='hidden' value='<?php echo $event['event_id']; ?>' name='id'>
		  <input type='hidden' value='<?php echo $event['event_id_ggl']; ?>' name='event_id_ggl'></td>
		  <td><input type='text' placeholder='Contact details'name='contact' size='30'  value='<?php echo $event['contact']; ?>' required></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Start time *</td>
		  <td><input type='datetime-local' placeholder='Start time' name='start' value='<?php echo date_create($event['start'])->format("Y-m-d\TH:i"); ?>' required></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>End time</td>
		  <td><input type='datetime-local' placeholder='Endtime (optional)' name='end' value='<?php echo date_create($event['end'])->format("Y-m-d\TH:i"); ?>'></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Location *</td>
		  <td>
			 <select name='location' required>
			 	<?php
			 	echo '<option value="hobbypark"'. ($event['location']=='hobbypark'?'selected':'') .'>Hobby Park</option>
			  		<option value="bridge-82"'. ($event['location']==' bridge-82'?'selected':'') .'>82</option>
			  		<option value="kreature"'. ($event['location']=='kreature'?'selected':'') .'>Kreature</option>
			  		<option value="other"'. ($event['location']=='other'?'selected':'') .'>Other</option>';
			 	?> 
			</select>
		  </td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Type *</td>
		  <td>
			 <select name='type' required>
			 <?php
			 	echo '<option value="party"'. ($event['type']=='party'?'selected':'') .'>Party</option>
			  		<option value="bachelor"'. ($event['type']=='bachelor'?'selected':'') .'>Bachelor party</option>
			  		<option value="walk"'. ($event['type']=='walk'?'selected':'') .'>Walk on</option>
			  		<option value="other"'. ($event['type']=='other'?'selected':'') .'>Other event</option>';
			 	?> 
			</select>
		  </td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Party size</td>
		  <td>
		  	<select name='size'>
		  	<?php
			 	echo '<option value="10-20"'. ($event['size']=='10-20'?'selected':'') .'>10-20</option>
			  		<option value="20-30"'. ($event['size']=='20-30'?'selected':'') .'>20-30</option>
			  		<option value="30-40"'. ($event['size']=='30-40'?'selected':'') .'>30-40</option>
			  		<option value="50-100"'. ($event['size']=='50-100'?'selected':'') .'>50-100</option>';
			 	?> 
			</select></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Package</td>
		  <td>
			 <select name='package'>
			 <?php
			 	echo '<option value="standard"'. ($event['package']=='standard'?'selected':'') .'>Standard</option>
			  		<option value="exp"'. ($event['package']=='exp'?'selected':'') .'>Experienced</option>
			  		<option value="birthday"'. ($event['package']=='birthday'?'selected':'') .'>Birthday</option>
			  		<option value="birthday_ex"'. ($event['package']=='birthday_ex'?'selected':'') .'>Birthday EXTRA</option>';
			 	?>
			</select>
		  </td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Deposit</td>
		  <td><input type='text' name='deposit' size='30' value='<?php echo $event['deposit']; ?>'></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Other info</td>
		  <td>
			<textarea type='text' name='other_info' form="new_event" rows="10" cols="30"><?php echo $event['other_info']; ?></textarea>
		  </td>
		</tr>
	</table>
	<button type='submit' name='update' value='Update event' class='btn btn-default'>Update event</button> 
	<a href='index.php' class='btn btn-default'>Cancel</a>
	</form>
</div>
<!-- tooltip handler -->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>