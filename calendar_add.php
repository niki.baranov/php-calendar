<?php
require_once('calendar_head.php');			// general functions
require_once("db/db-init.php");		// db connection

// check that user is logged in, even though already done in calendar_head.php
if($_SESSION['login2app'] != true){
	 $_SESSION['fail'] = "Noup, can't touch this.";
}
// user is logged in
else{
	$ggl_api_call = NULL;
	// get data from calendar_add_event.php:
	//$event_name	= (isset($_REQUEST['contact']))		? $_REQUEST['contact'] : '';
	$contact	= (isset($_REQUEST['contact']))		? $_REQUEST['contact'] : '';
	$start  	= (isset($_REQUEST['start']))		? $_REQUEST['start'] : '';
	$end		= (isset($_REQUEST['end']))		? $_REQUEST['end'] : '';
	$location	= (isset($_REQUEST['location']))	? $_REQUEST['location'] : '';
	//$type    	= (isset($_REQUEST['type']))		? $_REQUEST['type'] : '';
	//$size		= (isset($_REQUEST['size']))		? $_REQUEST['size'] : '';
	//$package	= (isset($_REQUEST['package']))		? $_REQUEST['package'] : '';
	//$deposit	= (isset($_REQUEST['deposit']))		? $_REQUEST['deposit'] : '';
	$other_info	= (isset($_REQUEST['other_info']))	? $_REQUEST['other_info'] : '';
	
	echo $start."<br>";
	echo $end."<br>";
	// create unique event id, start and end times for google api
	$event_id_ggl = md5($type.$location.$contact.time());
	$ggl_api_start = new DateTime($start);
	// set end time 1h later than start if no end time specified (or is 0)
	if($end == ''){
		$ggl_api_end = date_add(new DateTime($start),new DateInterval('PT1H'));
		$end = date_format($ggl_api_end,"Y-m-d\TH:i");
		}
	else{
		$ggl_api_end = new DateTime($end);
		}
	
	// process sql, use prepared statement
	$q_event = <<<newEvent
	INSERT INTO events
	(event_id_ggl, contact, start, end, location, type, size, package, deposit, other_info)
	VALUES (:event_id_ggl, :contact, :start, :end, :location, :type, :size, :package, :deposit, :other_info)
newEvent;
	
	$new_event = $db->prepare($q_event);
	// define parameters in the sql statement and execute sql query
	$new_event->execute(array(':event_id_ggl'=>$event_id_ggl,
				  ':contact'=>$contact,
				  ':start'=>$start,
				  ':end'=>$end,
				  ':location'=>$location,
				  ':type'=>$type,
				  ':size'=>$size,
				  ':package'=>$package,
				  ':deposit'=>$deposit,
				  ':other_info'=>$other_info));
	
	// google api auth check & call
	if (isset($_SESSION['access_token']) && $_SESSION['access_token'] && $event_id_ggl != '') {
		$client->setAccessToken($_SESSION['access_token']);
		$service = new Google_Service_Calendar($client);
		
		// populate event for Google API
		$event = new Google_Service_Calendar_Event(array(
		'id' => $event_id_ggl,
		'summary' => $contact.", ".$location.", ".$type,
		
		'start' => array(
		  	'dateTime' => date_format($ggl_api_start,"Y-m-d\TH:i:s\+02:00"),
		 ),
		 
		  'end' => array(
		  	'dateTime' => date_format($ggl_api_end,"Y-m-d\TH:i:s\+02:00"),
		),
		
		'attendees' => array(
			  array('email' => $contact)
		), 
		
		'description' => "Location: ".$location.";\nType: ".$type.";\nSize: ".$size.";\nContact: ".$contact.";\nPackage: ".$package.";\nDeposit: ".$deposit.";\nOther: ".$other_info,
		));
		
		// define which calendar to use
		$calendarId = 'primary';
		
		// send event to Google API
		$event = $service->events->insert($calendarId, $event);
		
		// set api call variable
		$ggl_api_call = "ok";
	}
	
	else{
		$ggl_api_call = NULL;
	}
	// google api call end
	
		if ($new_event->rowCount()!=0){
			if($ggl_api_call == "ok"){
				$_SESSION['success'] = "New event added!";
			}
			else{
				$_SESSION['success'] = "New event added to db!";
			}
		}
		else{
			$_SESSION['fail'] = "Sorry bru, Could not add new event!";
		}
}

header("Location: http://" . $_SERVER['HTTP_HOST']
		   . dirname($_SERVER['PHP_SELF']) . '/'
		   . "index.php");
?>