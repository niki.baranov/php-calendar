<?php
require_once('calendar_head.php');			// general functions
//require_once("db/db-init.php");		// db connection

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	$client->setAccessType("offline");
	$client->setAccessToken($_SESSION['access_token']);
  	$service = new Google_Service_Calendar($client);	
}
else{
	//echo "No API authentication available";
}

// redirect to login/oauth if access token not set
if(!isset($_SESSION['access_token']) && ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true))){  
  // redirect to login page
  header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
}

$page_title = "Calendar ";		// set page title

include('calendar_header.php');			// html header
include ('calendar.php');			// caledar generator
require_once ('showCalendar.class.php');	// load showCalendar class
// load available class files, not working due to google autoloader
//function __autoload($class_name) {
//       require_once $class_name . '.class.php';
//   }
?>

<body style="margin:0px; height:100%; width:100%">
<?php
include('calendar_menu.php');	// print top navigation

// page content starts below
?>	

<div class="container" style="margin:0px auto; padding:0px; width:100%">
<?php
showMsg();

if(isset($_GET['show-week'])){
	$show_calendar = new showCalendar();
	echo $show_calendar->showWeek();
}
elseif(isset($_GET['show-day'])){
	$show_calendar = new showCalendar();
	echo $show_calendar->showDay();
}
elseif(isset($_GET['id'])){
	$show_calendar = new showCalendar();
	echo $show_calendar->showEvent();
}
else{
	$calendar = new Calendar();
	echo $calendar->show();
}
?>
</div>
<!-- tooltip handler -->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>