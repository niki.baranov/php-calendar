<?php
// start session
session_start();
// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;

}
$page_title = "PHP Calendar Documentation";		// set page title
include('calendar_header.php');		// html header

?>
<body style="margin:0px; height:100%; width:100%">
<?php
include('calendar_menu.php');
?>
<div class='container' style='margin:30px auto;'>

<h1>The final plan</h1>
<h3>Documentation menu</h3>
<a href="plan1.php" >Preliminary plan</a><br>
<a href="final-plan.php" >Final plan</a><br>
<a href="final-report.php" >Final report</a><br>
<a href="time.php" >Time keeping / schedule</a><br>
<p></p>
<a href="php-syntax-highlighter.php" >Syntax highlighter Main</a><br>
<a href="ggl/php-syntax-highlighter.php" >Syntax highlighter GGL</a><br>
<a href="calendar.zip" >Project ZIP (without Google API Library)</a><br>
<a href="https://github.com/google/google-api-php-client/releases" > Google API Library</a><br>
<p></p>
<h2>The idea/purpose</h2>
<p>As a project for &ldquo;Web-palvelinohjelmointi, TTMS0900&rdquo; – course, I decided to create a calendar web application. Design, functionality as well as UI of the app are based on Google Calendar<strong>, with ability to synchronize event data from/to Google Calendar via Google API.</strong><br>
  Event data is stored in MySQL database. Application also features connection to Google API to allow for synchronization of calendar data via Google account and a bit of automation to a create &lsquo;hard&rsquo;-copy of current month,  week and day views.<br>
  Main reason for this web-application was a request from a friend to have a system that made it possible to use Google Calendar but would also allow for a customized look and feel fitting his specific business needs. In addition, I really wanted to get to know how date object is used and manipulated with PHP as well as get to know Google API a bit.<br>
  Apart from Google API and PHP date object, nothing was new or unfamiliar in this project. Due to close relations with the client, I already had a good understanding of possible needs, this helped with UI and UX aspect of application.</p>
<h2>API / libraries / frameworks</h2>
<p>Google API – Calendar and authentication.<br>
  PHP based calendar class – Found as tutorial from www.StarTutorial.com, by Xu Ding. Code was slightly modified to allow for some extra functionality.<br>
  Bootstrap framework was used for a base CSS, some customized CSS was added as well.</p>
<h2>Technologies</h2>
<p>Application is done cone mainly with PHP. Use of JS will be avoided. MySQL is used for data storage. CSS &amp; HTML will be used for data representation. Application will be setup on an existing server (LAMP) which also hosts client&rsquo;s website.</p>
<p><img src="php-calendar-tech.png" width="1192" height="578" alt=""/></p>
<h2>Use case diagram</h2>
<p><img src="php-calendar-use-case.png" width="1082" height="646" alt=""/></p>
<h2>DB structure</h2>
<p><img src="php-calendar-db.png" width="600" height="536" alt=""/></p>
</div>
<!-- tooltip handler -->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
<!-- Latest compiled and minified JavaScript -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
