<?php
$today = date_create(date("Y-m-d"));
$this_month = date("m");
$this_year = date("Y");
$next_month = date_add(date_create(date("Y-m")),new DateInterval('P1M'));
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Menu</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
			<a class="navbar-brand" href="index.php">DNA | Calendar </a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav">
			<li><a href="index.php">
				<span class="glyphicon glyphicon-home"></span> Home </a></li>
			<li><a href="calendar_add_event.php">
				<span class="glyphicon glyphicon-plus"></span> new event </a></li>
			<li><a href="show.php?date=<?php echo date("Y-m-d"); ?>&show-day">
				<span class="glyphicon glyphicon-th-list"></span> Today </a></li>
			<li><a href="show.php?date=<?php echo date("Y-m-d"); ?>&show-week">
				<span class="glyphicon glyphicon-th-large"></span> This week </a></li>
			<li><a href="show.php<?php echo "?month=".$this_month; ?>">
				<span class="glyphicon glyphicon-th"></span> This month </a></li>
			<li><a href="show.php<?php echo "?month=".$next_month->format('m')."&year=".$this_year; ?>">
				<span class="glyphicon glyphicon-th"></span> Next month </a></li>
		  </ul>

		  <ul class="nav navbar-nav navbar-right">
		  	
			<li><a href="documentation.php">Documentation</a></li>
			<li><a href="calendar_sync.php"><span class="glyphicon glyphicon-refresh"></span></a></li>
		  	<?php
		  	if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
		  		echo '<li><a  style="color:#0F0;"><span class="glyphicon glyphicon-user"></span> G+ </a></li>';
		  	}
		  	else{
		  		echo '<li><a href="ggl/oauth2callback.php" style="color:#F00;"><span class="glyphicon glyphicon-user"></span> Login G+ </a></li>';
		  	}
		  	// check for G+ login status
		  	?>
			<li><a href="calendar_logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout </a></li>
		  </ul>
		</div>
	</div>
</nav>