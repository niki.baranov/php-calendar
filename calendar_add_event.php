<?php
require_once('calendar_head.php');			// general functions
require_once("db/db-init.php");		// db connection

$page_title = "New event ";		// set page title
include('calendar_header.php');		// html header
?>

<!doctype html>
<body>
<?php
include('calendar_menu.php');	// print top navigation
// page content starts below
?>
<!-- main div -->
<div class="container" style="margin:10px auto">
<?php
showMsg();
?>
	<form method='post' action='calendar_add.php' id='new_event'>
	<table border='0' style='width:100%;'>
		<tr valign='top'>
		  <td align='right'>Contact *</td>
		  <td><input type='text' placeholder='Contact details'name='contact' size='30'  value='' required></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Start time *</td>
		  <td><input type='datetime-local' name='start' value='' required></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>End time</td>
		  <td><input type='datetime-local' name='end' value=''></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Location *</td>
		  <td>
			 <select name='location' required>
			  <option value="hobbypark">Hobby Park</option>
			  <option value="bridge-82">82</option>
			  <option value="kreature">Kreature</option>
			  <option value="other">Other</option>
			</select>
		  </td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Type *</td>
		  <td>
			 <select name='type' required>
			  <option value="party">Party</option>
			  <option value="bachelor">Bachelor party</option>
			  <option value="walk">Walk on</option>
			  <option value="other">Other event</option>
			</select>
		  </td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Party size</td>
		  <td>
		  	<select name='size'>
			  <option value="10-20">10-20</option>
			  <option value="20-30">20-30</option>
			  <option value="30-40">30-40</option>
			  <option value="50-100">50-100</option>
			</select></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Package</td>
		  <td>
			 <select name='package'>
			  <option value="standard">Standard</option>
			  <option value="exp">Experienced</option>
			  <option value="birthday">Birthday</option>
			  <option value="birthday_ex">Birthday EXTRA</option>
			</select>
		  </td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Deposit</td>
		  <td><input type='text' name='deposit' size='30' value=''></td>
		</tr>
		<tr valign='top'>
		  <td align='right'>Other info</td>
		  <td>
			<textarea type='text' name='other_info' form="new_event" rows="10" cols="30"></textarea>
		  </td>
		</tr>
	</table>
	<button type='submit' name='save' value='Add event' class='btn btn-default'>Add event</button> 
	<a href='index.php' class='btn btn-default'>Cancel</a>
	</form>	
	</div>

<!-- tooltip handler -->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>