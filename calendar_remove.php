<?php
require_once('calendar_head.php');			// general functions
require_once("db/db-init.php");		// db connection

// check that user is logged in, even though already done in calendar_head.php
if($_SESSION['login2app'] != true){
	 $_SESSION['fail'] = "Noup, can't tuoch this.";
}
// admin or id match
else{
$ggl_api_call = NULL;
// get data from calendar_add_event.php:
$event_id	= (isset($_REQUEST['id']))		? $_REQUEST['id'] : '';
$event_id_ggl	= (isset($_REQUEST['event_id_ggl']))		? $_REQUEST['event_id_ggl'] : '';

// process sql, use prepared statement
$q_event = <<<newEvent
DELETE FROM events
WHERE event_id=:event_id
newEvent;

$event = $db->prepare($q_event);
// define parameters in the sql statement
$event->execute(array(':event_id'=>$event_id));

// google api auth check & call
	if (isset($_SESSION['access_token']) && $_SESSION['access_token'] && $event_id_ggl != '') {
		$client->setAccessToken($_SESSION['access_token']);
		$service = new Google_Service_Calendar($client);
		
		// delete the event via api based on $event_id_ggl
		$calendarId = 'primary';
		$service->events->delete($calendarId, $event_id_ggl);
		
		// set api call variable
		$ggl_api_call = "ok";
	}
	
	else{
		$ggl_api_call = NULL;
	}
// google api call end

	if ($event->rowCount()!=0){
		if($ggl_api_call == "ok"){
			$_SESSION['success'] = "Event removed!";
		}
		else{
			$_SESSION['success'] = "Event removed from db!";
		}
	}
	else{
		$_SESSION['fail'] = "Sorry, could not remove this event!";
	}
}

header("Location: http://" . $_SERVER['HTTP_HOST']
		   . dirname($_SERVER['PHP_SELF']) . '/'
		   . "index.php");
?>