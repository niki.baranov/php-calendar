<?php
// Google Auth
//require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');
require_once ('calendar.php');			// caledar generator
require_once ('showCalendar.class.php');	// load showCalendar class
require_once ('calendar_head.php');		// basic functions

//$client = new Google_Client();
//$client->setAuthConfig('/home/dnapaint/etc/calendar/client_secret.json');
//$client->addScope(Google_Service_Calendar::CALENDAR);
//$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);

// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;

}
$page_title = "Calendar for  ";		// set page title
include('calendar_header.php');		// html header

?>
<body style="margin:0px; height:100%; width:100%">
<?php
include('calendar_menu.php');echo showMsg();
?><div class='container' style='margin:0px auto; padding:0px; width:100%'>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: date in C:\xampp\htdocs\php-calendar\showCalendar.class.php on line <i>20</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.4004</td><td bgcolor='#eeeeec' align='right'>416584</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='C:\xampp\htdocs\php-calendar\calendar_create_hard_copy.php' bgcolor='#eeeeec'>...\calendar_create_hard_copy.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.4014</td><td bgcolor='#eeeeec' align='right'>529720</td><td bgcolor='#eeeeec'>ShowCalendar->__construct(  )</td><td title='C:\xampp\htdocs\php-calendar\calendar_create_hard_copy.php' bgcolor='#eeeeec'>...\calendar_create_hard_copy.php<b>:</b>74</td></tr>
</table></font>
<div class="table-responsive">
			<div class="box">
				<div class="header"><a class="prev" href="show.php?date=2020-02-10&show-day">
					<span class="glyphicon glyphicon-step-backward"></span> Prev</a>
					<span class="title">Tue 11th of February</span>
				<a class="next" href="show.php?date=2020-02-12&show-day">
					Next <span class="glyphicon glyphicon-step-forward"></span></a></div>
			</div>
			<table class="table table-striped weekly">
				<tr>
					<th>Hour</th>
					<th>Tue 11th</th>
				</tr><tr><td>00</td><td></td></tr><tr><td>01</td><td></td></tr><tr><td>02</td><td></td></tr><tr><td>03</td><td></td></tr><tr><td>04</td><td></td></tr><tr><td>05</td><td></td></tr><tr><td>06</td><td></td></tr><tr><td>07</td><td></td></tr><tr><td>08</td><td></td></tr><tr><td>09</td><td></td></tr><tr><td>10</td><td></td></tr><tr><td>11</td><td></td></tr><tr><td>12</td><td></td></tr><tr><td>13</td><td></td></tr><tr><td>14</td><td></td></tr><tr><td>15</td><td></td></tr><tr><td>16</td><td></td></tr><tr><td>17</td><td></td></tr><tr><td>18</td><td></td></tr><tr><td>19</td><td></td></tr><tr><td>20</td><td></td></tr><tr><td>21</td><td></td></tr><tr><td>22</td><td></td></tr><tr><td>23</td><td></td></tr></table></div>	</div>
	<!-- tooltip handler -->
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
	</html>
	