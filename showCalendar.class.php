<?php
/**
*ShowCalendar.class.php
*Methods: showDay, showWeek, showEvent, getEvent, createNavigation, createTableHeaders
*@author  Niki Baranov
*@email   niki.baranov@gmail.com
**/

// showCalendar
class ShowCalendar {
	protected $_date 			= NULL;
	protected $_month			= NULL;
	//protected $_href			= NULL;
	protected $_format  		= NULL;
	protected $_event_id	= NULL;

 function __construct() {
	// create date from url or use current date
    	$this->_date    = date_create(isset($_GET['date']) ? $_GET['date'] : '');
	if ($_GET['date']==''){	
		$this->_date = date_add($this->_date, new DateInterval('PT8H'));
	}
	// get event id from url or use ''
	$this->_event_id = isset($_GET['id']) ? $_GET['id'] : '';
	// create href based on current url;
	//$this->_href 	= htmlentities($_SERVER['PHP_SELF']);
	// set manually because of hardcopy
	$this->_href = "show.php";
	$this->_format  = "%02d";
  }
 
 function __destruct() {
    $this->_date    = NULL;
    $this->_week    = NULL;
	$this->_href	= NULL;
	$this->_format  = NULL;
 }
 
 public function showDay(){
 	
	// db connection
	require_once("db/db-init.php");
	// crete day view based on provided data
	$day_view = 
		'<div class="table-responsive">
			<div class="box">
				<div class="header">'.
					$this->createNavigation('day').
				'</div>
			</div>
			<table class="table table-striped weekly">
				<tr>
					<th>Hour</th>
					<th>'.$this->_date->format("D jS").'</th>
				</tr>';
				
	// add to day_view new rows&cells for each hour
	// 24 calls to db to locate and populate correct day and hour of event
	$this->_date = date_create($this->_date->format("Y-m-d H:i:s"));
	$i = 0;
	while($i <= 23){
		// add hour count
		$day_view .= "<tr><td>".sprintf($this->_format, $i)."</td>";
		$day_view .= "<td>";
			
		// find events for the day
		$events = $this->getEvents($db,$this->_date->format("Y-m-d"),'');

		// find events for specific hour, add to week view
		foreach($events as $event){
			$event_time = date_create($event['start']);
			if($event_time->format("Y-m-d H") == $this->_date->format("Y-m-d H")){
				$day_view .=  "<div class='day'>
									<a href='".$this->_href."?id=".$event['event_id']."' class='event-".$event['location']."'>
									<span class='event'>".$event['contact']."</span>
									</a>
								</div>";
			}
		}
		
		$day_view .= "</td>";		
		$day_view .= "</tr>";
		
	// increase hour value of the hour by 1
	$this->_date = date_add($this->_date, new DateInterval('PT1H'));	
	$i++;
	}
	
	$day_view .= 	'</table>';
	$day_view .= 	'</div>';
	// return the whole daily view table with headers and navigation
	return $day_view;
 }

// showWeek start
 public function showWeek(){
	// db connection
	require_once("db/db-init.php");
	// find the date of monday for selected week by counting 'down' to from the date value
	// check whether date value is monday
	while($this->_date->format("D")!='Mon'){
		// subtract one day from date
		date_sub($this->_date, new DateInterval('P1D'));
	}
	
	// introduce events
	//$events = array();

	$i = 0;		
	$this->_date = date_create($this->_date->format("Y-m-d H:i:s"));
	 
	// crete week view based on provided data
	$week_view = 
		'<div class="table-responsive">
			<div class="box">
				<div class="header">'.
					$this->createNavigation('week').
				'</div>
			</div>
			<table class="table table-striped weekly">
				<tr>'.
					$this->createTableHeaders('week').
				'</tr>';
	 
	// print hour and events for that hour
	// 24 x 7 calls to db to locate and populate correct day and hour of event
	while($i <= 23){

		// add hour count
		$week_view .= "<tr><td>".sprintf($this->_format, $i)."</td>";

		// reset day count
		$week_day_i = 0;
		
		// as long as week_day is  less or equal to 6 go through events array
		while($week_day_i<=6){
			$week_view .= "<td>";
			
			// find events for the day
			$events = $this->getEvents($db,$this->_date->format("Y-m-d"),'');

			// find events for specific hour, add to week view
			foreach($events as $event){
				$event_time = date_create($event['start']);
				if($event_time->format("Y-m-d H") == $this->_date->format("Y-m-d H")){
					$week_view .=  "<div class='day'>
							<a href='".$this->_href."?id=".$event['event_id']."' class='event-".$event['location']."'>
							<span class='event'>".($event['contact']!=''?$event['contact']:$event['event_name'])."</span>
							</a>
							</div>";
				}
			}
			$week_day_i++;
			$week_view .= "</td>";
			
			// add day count
			$this->_date = date_add($this->_date, new DateInterval('P1D'));

			// when reaching sunday reset date value of week_day
			if($week_day_i==7){
				$this->_date = date_sub($this->_date, new DateInterval('P7D'));
			}
		}
		$week_view .= "</tr>";
	// increase hour value of the hour by 1
	$this->_date = date_add($this->_date, new DateInterval('PT1H'));	
	$i++;
	}
	
	$week_view .= 		'</table>';
	$week_view .= 	'</div>';
	// return the whole weekly view table with headers and navigation
	return $week_view;
}
// showWeek end
	
// create navigation start
private function createNavigation($type){
	// if type is week
	if($type === 'week'){
		// set prev & next mondays to use in navigation
		$prev_week	= date_format(date_sub(clone $this->_date, new DateInterval('P7D')), "Y-m-d");
		$next_week	= date_format(date_add(clone $this->_date, new DateInterval('P7D')), "Y-m-d");

		return '<a class="prev" href="'.$this->_href.'?date='.$prev_week.'&show-week">
					<span class="glyphicon glyphicon-step-backward"></span> Prev</a>
					<span class="title">Week '.$this->_date->format("W F").'</span>
				<a class="next" href="'.$this->_href.'?date='.$next_week.'&show-week">
					Next <span class="glyphicon glyphicon-step-forward"></span></a>';
		}
	// else show day nav
	else{
		// set prev & next days to use in navigation
		$prev_day	= date_format(date_sub(clone $this->_date, new DateInterval('P1D')), "Y-m-d");
		$next_day	= date_format(date_add(clone $this->_date, new DateInterval('P1D')), "Y-m-d");

		return '<a class="prev" href="'.$this->_href.'?date='.$prev_day.'&show-day">
					<span class="glyphicon glyphicon-step-backward"></span> Prev</a>
					<span class="title">'.$this->_date->format("D jS \of F").'</span>
				<a class="next" href="'.$this->_href.'?date='.$next_day.'&show-day">
					Next <span class="glyphicon glyphicon-step-forward"></span></a>';
		
	}
}
// create navigation end
	
// createTableHeaders start
// used to generate week view table headers
private function createTableHeaders(){
		// define dates for each day of the week by cloning monday date and adding n days
		// done by cloning to preserve initial value of 'monday' date
		$monday		=date_create($this->_date->format("Y-m-d"));
		$tuesday 	=date_add(clone $this->_date, new DateInterval('P1D'));
		$tuesday 	=date_add(clone $this->_date, new DateInterval('P1D'));
		$wednesday 	=date_add(clone $this->_date, new DateInterval('P2D'));
		$thursday 	=date_add(clone $this->_date, new DateInterval('P3D'));
		$friday 	=date_add(clone $this->_date, new DateInterval('P4D'));
		$saturday 	=date_add(clone $this->_date, new DateInterval('P5D'));
		$sunday 	=date_add(clone $this->_date, new DateInterval('P6D'));

		// return table headers with dates
		// provides links to each day
		return '<th>Hour</th>
				<th><a href="'.$this->_href.'?date='.$monday->format("Y-m-d").'&show-day" class="daynum">'.
					$monday->format("D jS").'</a></th>
				<th><a href="'.$this->_href.'?date='.$tuesday->format("Y-m-d").'&show-day" class="daynum">'.
					$tuesday->format("D jS").'</a></th>
				<th><a href="'.$this->_href.'?date='.$wednesday->format("Y-m-d").'&show-day" class="daynum">'.
					$wednesday->format("D jS").'</a></th>
				<th><a href="'.$this->_href.'?date='.$thursday->format("Y-m-d").'&show-day" class="daynum">'.
					$thursday->format("D jS").'</a></th>
				<th><a href="'.$this->_href.'?date='.$friday->format("Y-m-d").'&show-day" class="daynum">'.
					$friday->format("D jS").'</a></th>
				<th><a href="'.$this->_href.'?date='.$saturday->format("Y-m-d").'&show-day" class="daynum">'.
					$saturday->format("D jS").'</a></th>
				<th><a href="'.$this->_href.'?date='.$sunday->format("Y-m-d").'&show-day" class="daynum">'.
					$sunday->format("D jS").'</a></th>';
}
// createTableHeaders end

// showEvent start
// gets events data from db via getEvents, passed db, date and event_id (only db and event_id are relevant)
public function showEvent(){
	// db connection
	require_once("db/db-init.php");
	$events = $this->getEvents($db, $this->_date->format("Y-m-d"),$this->_event_id);
	$event_location = NULL;
	$event_type = NULL;
	
	if ($events->rowCount()<=0){
		$_SESSION['fail'] = "Event not found my bru!";
		$event = NULL;
	}
	else{
		$event = $events->fetch(PDO::FETCH_ASSOC);
		// specify location
		switch ($event['location']) {
		    case "hobbypark":
		        $event_location = "Hobbypark";
		        break;
		    case "kreature":
		        $event_location = "Kreature";
		        break;
		    case "bridge-82":
		        $event_location = "Bridge 82";
		        break;
		    case "other":
		        $event_location = "Other location";
		        break;
		    default:
		        $event_location  = "not specified";
		}
		// specify type
		switch ($event['type']) {
		    case "party":
		        $event_type = "Party";
		        break;
		    case "bachelor":
		        $event_type = "Bachelors party";
		        break;
		    case "walk":
		        $event_type = "Walk on";
		        break;
		    case "other":
		        $event_type  = "Other type";
		        break;
		    default:
		        $event_type =  "not specified";
		}
		echo "\n<div class='table-responsive'>";
		echo "\n\t<table class='table table-striped'>";
		
		echo "\n\t\t\n\t\t<tr><td class='event_details'></td><td><b>".($event['event_name']!=''?$event['event_name']:$event_type." at ".$event_location." for ".$event['size'])."</b></td>\n\t\t</tr>";
		
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Starts</td><td>".$event['start']."</td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Ends</td><td>".$event['end']."</td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Contact</td><td>".$event['contact']."</td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Location</td><td><b>".$event_location."</b></td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Type</td><td>".$event_type."</td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Group size</td><td>".$event['size']."</td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Package</td><td>".$event['package']."</td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Deposit</td><td>".$event['deposit']."</td>\n\t\t</tr>";
		echo "\n\t\t\n\t\t<tr><td class='event_details'>Other info</td><td>".$event['other_info']."</td>\n\t\t</tr>";
		echo "<tr><td colspan='2'>";
		echo "<table><tr><td>";
		echo "<form method='post' action='calendar_edit_event.php'>
			<input type='hidden' value='".$event['event_id']."' name='id'>
			<input type='hidden' value='".$event['event_id_ggl']."' name='event_id_ggl'>
			<button type='submit' name='edit' value='Edit event' class='btn btn-default'>Edit event</button>
			</form></td>";
		echo "<td><form method='post' action='calendar_remove.php'>
			<input type='hidden' value='".$event['event_id']."' name='id'>
			<input type='hidden' value='".$event['event_id_ggl']."' name='event_id_ggl'>
			<button type='submit' name='delete' value='Delete event' class='btn btn-default'>Delete event</button>
			</form></td></tr></table>";
			
		echo "</td></tr></table>";
		echo "</div>";
	}
}	
// showEvent end
 	
// getEvents start
private function getEvents($db, $date, $event_id){
	if($event_id != ''){
		$q_events = <<<getEvents
		SELECT * 
		FROM events 
		WHERE event_id=:event_id 
getEvents;
		$events = $db->prepare("$q_events");
		// define parameters in the sql statement
		$events->execute(array(':event_id'=>$event_id));
   		return $events; 
	}
	else{
		$q_events = <<<getEvents
		SELECT * 
		FROM events 
		WHERE DATE_FORMAT(start, '%Y-%m-%d')=DATE_FORMAT(:day, '%Y-%m-%d')
		ORDER BY start
getEvents;
		$events = $db->prepare("$q_events");
		// define parameters in the sql statement
		$events->execute(array(':day'=>$date));
	  	return $events;
	}
}
// getEvents end

}
// end showCalendar
?>