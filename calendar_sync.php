<?php
require_once('calendar_head.php');			// general functions
require_once("db/db-init.php");		// db connection

// redirect to login/oauth if access token not set
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	$client->setAccessType("offline");
	$client->setAccessToken($_SESSION['access_token']);
  	$service = new Google_Service_Calendar($client);	
}else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/calendar/google-api-php-client-2.2.0/ggl/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  }

// google API connection
	// Print the next 30 events in the user's calendar.
$calendarId = 'primary';
$optParams = array(
  'maxResults' => 30,
  'orderBy' => 'startTime',
  'singleEvents' => TRUE,
  'timeMin' => date('c'),
);

$results = $service->events->listEvents($calendarId, $optParams);

// check that at least some events were found
if (count($results->getItems()) == 0) {
	print "No upcoming events found.\n";
}
else {
	print "Upcoming events:\n";
	  
	  foreach ($results->getItems() as $event) {
		$event_id_ggl	= '';
		$contact	= '';
		$start		= '';
		$end		= '';
		$location	= '';
		$type		= '';
		$size		= '';
		$package	= '';
		$deposit	= '';
		$other_info	= '';
	  
		$start	= $event->start->dateTime;
		$end	= $event->end->dateTime;
		
		if (empty($start)) {
		  $start = $event->start->date;
		}
		if (empty($end)) {
		  $end = $event->end->date;
		}
		 // list attendees
		$attendees = NULL;
		foreach($event->attendees as $attendee){
			$attendees .= $attendee['email'].",";
		}
		$description = array();
		$re_pattern = "/(\w+\:\s?)(\b((?!=|\;).)+(.)\b)/";
		
		preg_match_all($re_pattern, $event['description'], $matches, PREG_SET_ORDER, 0);
		$i = 0;
		foreach($matches as $match){
			$description[] = array("detail_name" => $matches[$i][1],
					"detail_data" => (isset($matches[$i][1])?$matches[$i][2]:''));
			$i++;
		}
		
		// populate possible data from 'description' field
		foreach($description as $detail){
			if($detail['detail_name'] == "Location:"){
				//$location = $detail['detail_data'];
			}
			switch ($detail['detail_name']) {
			    case "Location: ":
			        $location = $detail['detail_data'];
			        break;
			    case "Type: ":
			        $type = $detail['detail_data'];
			        break;
			    case "Size: ":
			        $size = $detail['detail_data'];
			        break;
			    case "Contact: ":
			        $contact = $detail['detail_data'];
			        break;
			    case "Package: ":
			        $package = $detail['detail_data'];
			        break;
			    case "Deposit: ":
			        $deposit = $detail['detail_data'];
			        break;
			    case "Other: ":
			        $other_info = $detail['detail_data'];
			        break;
			    default:
			        $other_info = $detail['detail_data'];
			}
		}
		
	  	$event_name = $event->getSummary();
	  	$event_id_ggl = $event->id;
	  	if ($attendees != ''){
	  		$contact = $attendees; /*var_dump($event->attendees)*/
	  	}
	  	elseif($contact != ''){
	  		$contact = $contact;
	  	}
	  	else{
	  		$plus = new Google_Service_Plus($client);
			$person = $plus->people->get('me');
			$contact = ($person['emails'][0]['value']);
	  	}
	  	// other usable API data
		//$description = $event['description']; description without cutting it to pieces
	  	//$location = $event['location']; actual map location
		
		//check if  db has event with the same id, if so update
		$check_id_q = "SELECT event_id_ggl FROM events WHERE event_id_ggl = :event_id_ggl";
		$event_check = $db->prepare($check_id_q);
		$event_check->execute(array(':event_id_ggl'=>$event_id_ggl));
		
		if ($event_check->rowCount()!=0){
			$update_event_q = "UPDATE events
						SET 
						event_name=:event_name,
						contact=:contact, 
						start=:start, 
						end=:end, 
						location=:location, 
						type=:type, 
						size=:size, 
						package=:package, 
						deposit=:deposit, 
						other_info=:other_info
						WHERE event_id_ggl=:event_id_ggl";
			$update_event = $db->prepare($update_event_q);

			// define parameters in the sql statement
			$update_event->execute(array(':event_id_ggl'=>$event_id_ggl,
						  ':event_name'=>$event_name,
						  ':contact'=>$contact,
						  ':start'=>$start,
						  ':end'=>$end,
						  ':location'=>$location,
						  ':type'=>$type,
						  ':size'=>$size,
						  ':package'=>$package,
						  ':deposit'=>$deposit,
						  ':other_info'=>$other_info));	
		}
		
		// if new event, write new entry
		else{
		$q_event = "INSERT INTO events
					(event_id_ggl, 
					 event_name,
					 contact, 
					 start, 
					 end, 
					 location, 
					 type, 
					 size, 
					 package, 
					 deposit, 
					 other_info)
				VALUES (:event_id_ggl, 
					:event_name,
					:contact, 
					:start, 
					:end, 
					:location, 
					:type, 
					:size, 
					:package, 
					:deposit, 
					:other_info)";
		
		$synced_event = $db->prepare($q_event);
		
		// define parameters in the sql statement and execute sql query
		$synced_event->execute(array(':event_id_ggl'=>$event_id_ggl,
					  ':event_name'=>$event_name,
					  ':contact'=>$contact,
					  ':start'=>$start,
					  ':end'=>$end,
					  ':location'=>$location,
					  ':type'=>$type,
					  ':size'=>$size,
					  ':package'=>$package,
					  ':deposit'=>$deposit,
					  ':other_info'=>$other_info));
		// sql query
		}
	}
}

// redirect back to index
header("Location: http://" . $_SERVER['HTTP_HOST']
		   . dirname($_SERVER['PHP_SELF']) . '/'
		   . "index.php");
?>