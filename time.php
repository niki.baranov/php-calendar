<?php
// start session
session_start();
// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;

}
$page_title = "PHP Calendar Documentation";		// set page title
include('calendar_header.php');		// html header

?>
<body style="margin:0px; height:100%; width:100%">
<?php
include('calendar_menu.php');
?>
<div class='container' style='margin:30px auto;'>
<h3>Documentation menu</h3>
<a href="plan1.php" >Preliminary plan</a><br>
<a href="final-plan.php" >Final plan</a><br>
<a href="final-report.php" >Final report</a><br>
<a href="time.php" >Time keeping / schedule</a><br>
<p></p>
<a href="php-syntax-highlighter.php" >Syntax highlighter Main</a><br>
<a href="ggl/php-syntax-highlighter.php" >Syntax highlighter GGL</a><br>
<a href="calendar.zip" >Project ZIP (without Google API Library)</a><br>
<a href="https://github.com/google/google-api-php-client/releases" > Google API Library</a><br>
<p></p>
<table cellspacing="0" cellpadding="0">
  <col width="157">
  <col width="87">
  <col width="87">
  <col width="527">
  <tr>
    <td width="157">Date</td>
    <td width="87">Hours</td>
    <td width="87"></td>
    <td width="527">Task</td>
  </tr>
  <tr>
    <td>14.11 - 15.11</td>
    <td>1.0</td>
    <td>h</td>
    <td>Preliminary plan, project confirmation</td>
  </tr>
  <tr>
    <td></td>
    <td>5.0</td>
    <td>h</td>
    <td>Revision of the plan based on new needs &amp; information (continuous   task)</td>
  </tr>
  <tr>
    <td></td>
    <td>8.0</td>
    <td>h</td>
    <td>Adjustment of UI (continuous)</td>
  </tr>
  <tr>
    <td></td>
    <td>3.0</td>
    <td>h</td>
    <td>Testing (continuous task)</td>
  </tr>
  <tr>
    <td>16.11 - 18.11</td>
    <td>6.0</td>
    <td>h</td>
    <td>Introduction to Google API (Calendar / G+)</td>
  </tr>
  <tr>
    <td>17.11</td>
    <td>0.5</td>
    <td>h</td>
    <td>Setting up test environment</td>
  </tr>
  <tr>
    <td>17.11</td>
    <td>0.5</td>
    <td>h</td>
    <td>Inital app structure plan</td>
  </tr>
  <tr>
    <td>17.11</td>
    <td>0.5</td>
    <td>h</td>
    <td>Initial app UI / design plan</td>
  </tr>
  <tr>
    <td>17.11</td>
    <td>2.0</td>
    <td>h</td>
    <td>Finding tools to generate 30 day view</td>
  </tr>
  <tr>
    <td>18.11</td>
    <td>0.5</td>
    <td>h</td>
    <td>Database plan / creation, adjusting of db</td>
  </tr>
  <tr>
    <td>20.11</td>
    <td>4.0</td>
    <td>h</td>
    <td>Monthly view, event placement</td>
  </tr>
  <tr>
    <td>21.11</td>
    <td>3.0</td>
    <td>h</td>
    <td>week view, day view, event view</td>
  </tr>
  <tr>
    <td>22.11</td>
    <td>2.0</td>
    <td>h</td>
    <td>Event management</td>
  </tr>
  <tr>
    <td>23.11</td>
    <td>0.5</td>
    <td>h</td>
    <td>Setting up live environment </td>
  </tr>
  <tr>
    <td>23.11</td>
    <td>1.0</td>
    <td>h</td>
    <td>Creating presentation of the project</td>
  </tr>
  <tr>
    <td>24.11</td>
    <td>0.5</td>
    <td>h</td>
    <td>Presentation of project to class</td>
  </tr>
  <tr>
    <td>25.11</td>
    <td>4.0</td>
    <td>h</td>
    <td>Google API login/authentication </td>
  </tr>
  <tr>
    <td>25.11</td>
    <td>1.0</td>
    <td>h</td>
    <td>Adding date change system to views</td>
  </tr>
  <tr>
    <td>29.11</td>
    <td>6.0</td>
    <td>h</td>
    <td>Google API Sync</td>
  </tr>
  <tr>
    <td>27.11 - 29.11</td>
    <td>10.0</td>
    <td>h</td>
    <td>Converting to Class/OOP</td>
  </tr>
  <tr>
    <td>28.11</td>
    <td>2.0</td>
    <td>h</td>
    <td>Final structure, functions</td>
  </tr>
  <tr>
    <td>26.11 - 29.11</td>
    <td>5.0</td>
    <td>h</td>
    <td>Final report, &amp; documentation &amp; diagrams</td>
  </tr>
  <tr>
    <td>29.11</td>
    <td></td>
    <td></td>
    <td>Project release</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>65.0</td>
    <td>h</td>
    <td></td>
  </tr>
</table>
</div>
<!-- tooltip handler -->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
