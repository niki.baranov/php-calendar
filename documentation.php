<?php
// start session
session_start();
// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;

}
$page_title = "PHP Calendar Documentation";		// set page title
include('calendar_header.php');		// html header

?>
<body style="margin:0px; height:100%; width:100%">
<?php
include('calendar_menu.php');
?>
<div class='container' style='margin:30px auto;'>
<h1>PHP Calendar with Google API connection</h1>

<h3>Documentation menu</h3>
<a href="plan1.php" >Preliminary plan</a><br>
<a href="final-plan.php" >Final plan</a><br>
<a href="final-report.php" >Final report</a><br>
<a href="time.php" >Time keeping / schedule</a><br>
<p></p>
<a href="php-syntax-highlighter.php" >Syntax highlighter Main</a><br>
<a href="ggl/php-syntax-highlighter.php" >Syntax highlighter GGL</a><br>
<a href="calendar.zip" >Project ZIP (without Google API Library)</a><br>
<a href="https://github.com/google/google-api-php-client/releases" > Google API Library</a><br>
<p></p>
<a href="calendar_create_hard_copy.php" >Create This months 'hard'-copy</a><br>
<a href="calendar_create_hard_copy.php?show-week" >Create This weekls 'hard'-copy</a><br>
<a href="calendar_create_hard_copy.php?show-day" >Create Today's 'hard'-copy</a><br>
<p></p>
<p>@author: Nikita Baranov, C9710</p>
<p>@course: Web-palvelinohjelmointi : TTMS0900</p>
<p>@date: 30.11.2017</p>


</div>
<!-- tooltip handler -->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	});
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
