<?php
// Google Auth
//require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');

// start session
session_start();
// Google Auth
//$client = new Google_Client();
//$client->setAuthConfig('/home/dnapaint/etc/calendar/client_secret.json');
//$client->addScope(Google_Service_Calendar::CALENDAR);
//$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);

// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;
}

// debug not active yet
$_SESSION['debug'] = 1;			// change to 1 to activate debug
if($_SESSION['debug'] == 1){
	error_reporting(E_ALL);		// activate PHP error reporting if debug is 1
}
// debug

// show message
function showMsg(){
	if(isset($_SESSION['fail'])){
		echo '<div class="alert alert-danger" role="alert">'.$_SESSION['fail'].'</div>';
		unset($_SESSION['fail']);
	}
	if(isset($_SESSION['success'])){
		echo '<div class="alert alert-success" role="alert">'.$_SESSION['success'].'</div>';
		unset($_SESSION['success']);
	}
}	
// show message
?>