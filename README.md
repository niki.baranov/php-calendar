# PHP Calendar with Google API connection

As a project for “Web-palvelinohjelmointi, TTMS0900” – course, I decided to create a calendar web application with ability to synchronize event data from/to Google Calendar via Google API. Design, functionality as well as UI of the app are based on Google Calendar.

Main reason for this web-application was a request from a friend to have a system that made it possible to use Google Calendar but would also allow for a customized look and feel fitting his specific business needs.

This App is provided as is. It was originally created in late 2017 using a Google+ authentication to provide a sync capability with Google Calendar. Google+ is no longer supported by Google so authentication method has been changed to "up-to-date" version of Google Authentication (v.2020)

**The code found here has been altered to a more generic use.**

## TODO

    Adding users on the login screen.
    Reconfiguration of Google Authentication API

## Requirements

This App uses PHP for logic, Bootstrap for UI and  MySQL for database.

Calendar.class is based on a tutorial from www.StarTutorial.com, by Xu Ding.

Application was created and tested on PHP 7.0.25 and MySQL 5.5.32 running on Apache 2.0 server.

### Update 2020

    In 2020 the app was updated to use the new Google Authentication using:
    XAMP v.3.2.4
    Apache 2.4.41 (Win64)
    MySQL/MariaDB 10.4.11
    PHP 7.4.1.

[Google API Library](https://github.com/googleapis/google-api-php-client/releases)

## DB structure

User information is saved in ``cal_usrs`` table as ``username`` and ``passwd``

The cal_user table should consist of the following columns: unique var32 ``username``, var64 ``password``, auto incremented unique primary int11 ``uid``, int11 ``active`` and current timestamp ``create_date``.

The following SQL code should be able to create a new user (change details)

``INSERT INTO `cal_usrs` (`username`, `password`, `active`) VALUES ('your_email', 'password_hash_based_on_PasswordLib', '1');``.

Note that create_date should be automatically populated, this is information that is not necessary but good to have, the UID is not used by the system but it could be good to have for debugging later on, it should also be automatically incremented and populated in the db upon user creation.
Keeping username aka email as unique is also useful.

Added later...
cal_users structure
events structure

## Passwords and PasswordLib.phar

This app uses PasswordLib.phar to securely handle password hashes on the server.

To test that the library works properly you can use the following PHP code, it will print the hashed version of whatever you provide as "password". It will also check the length of the hash and verify that it works "both ways":

    <?php
        require_once 'PasswordLib.phar';
        $lib = new PasswordLib\PasswordLib();
        $password = "123";
        $hash = $lib->createPasswordHash($password,  '$2a$', array('cost' => 12));
        echo "$hash<br>";
        $boolean = $lib->verifyPasswordHash($password, $hash);
        if ($boolean) echo "Password $password is correct<br>";
        echo strlen($hash);
      ?>

Good articles on this can be found at:

[Password hashing in php - Sitepoint](http://www.sitepoint.com/password-hashing-in-php/)

[How do you use bcrypt for hashing passwords in php - Sitepoint](http://stackoverflow.com/questions/4795385/how-do-you-use-bcrypt-for-hashing-passwords-in-php)

[Hashing security - Crackstation](https://crackstation.net/hashing-security.htm)

## Communication & Use case

The following images representing the communication and use cases for the app

![image](img/php-calendar-tech.png)
![image](img/php-calendar-use-case.png)

## Original mockup & end result

![image](img/php-calendar-mockup.jpg)
![image](img/php-calendar-real.jpg)