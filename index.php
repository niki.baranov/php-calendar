<?php
// Google Auth
//require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');
require_once ('calendar.php');			// caledar generator
require_once ('showCalendar.class.php');	// load showCalendar class
require_once ('calendar_head.php');		// basic functions

//$client = new Google_Client();
//$client->setAuthConfig('/home/dnapaint/etc/calendar/client_secret.json');
//$client->addScope(Google_Service_Calendar::CALENDAR);
//$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);

// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;

}
$page_title = "Calendar for  ";		// set page title
include('calendar_header.php');		// html header

?>
<body style="margin:0px; height:100%; width:100%">
<?php
include('calendar_menu.php');echo showMsg();
?><div class='container' style='margin:0px auto; padding:0px; width:100%'>
<div id="calendar"><div class="box"><div class="header"><a class="prev" href="show.php?month=01&year=2020"><span class="glyphicon glyphicon-step-backward"></span> Prev</a> <span class="title">February 2020</span> <a class="next" href="show.php?month=03&year=2020">Next <span class="glyphicon glyphicon-step-forward"></span></a></div></div><div class="box-content"><div class="label"><div class="start title title">Mon</div><div class="start title title">Tue</div><div class="start title title">Wed</div><div class="start title title">Thu</div><div class="start title title">Fri</div><div class="start title title">Sat</div><div class="start title title">Sun</div></div><div class="clear"></div>
		<div class='dates'>
			<div id="" 
				class=" start   day">
				<h2 class="daynum"></h2></div>
			<div id="" 
				class="   day">
				<h2 class="daynum"></h2></div>
			<div id="" 
				class="   day">
				<h2 class="daynum"></h2></div>
			<div id="" 
				class="   day">
				<h2 class="daynum"></h2></div>
			<div id="" 
				class="   day">
				<h2 class="daynum"></h2></div>
			<div id="2020-02-01" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-1&show-day class="daynum">1</a></h2></div>
			<div id="2020-02-02" 
				class=" end   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-2&show-day class="daynum">2</a></h2></div>
			<div id="2020-02-03" 
				class=" start   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-3&show-day class="daynum">3</a></h2></div>
			<div id="2020-02-04" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-4&show-day class="daynum">4</a></h2></div>
			<div id="2020-02-05" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-5&show-day class="daynum">5</a></h2></div>
			<div id="2020-02-06" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-6&show-day class="daynum">6</a></h2></div>
			<div id="2020-02-07" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-7&show-day class="daynum">7</a></h2></div>
			<div id="2020-02-08" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-8&show-day class="daynum">8</a></h2></div>
			<div id="2020-02-09" 
				class=" end   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-9&show-day class="daynum">9</a></h2></div>
			<div id="2020-02-10" 
				class=" start   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-10&show-day class="daynum">10</a></h2></div>
			<div id="2020-02-11" 
				class="  today  day">
				<h2 class="daynum-today"><a href=show.php?date=2020-02-11&show-day class="daynum">11</a></h2></div>
			<div id="2020-02-12" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-12&show-day class="daynum">12</a></h2></div>
			<div id="2020-02-13" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-13&show-day class="daynum">13</a></h2></div>
			<div id="2020-02-14" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-14&show-day class="daynum">14</a></h2></div>
			<div id="2020-02-15" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-15&show-day class="daynum">15</a></h2></div>
			<div id="2020-02-16" 
				class=" end   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-16&show-day class="daynum">16</a></h2></div>
			<div id="2020-02-17" 
				class=" start   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-17&show-day class="daynum">17</a></h2></div>
			<div id="2020-02-18" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-18&show-day class="daynum">18</a></h2></div>
			<div id="2020-02-19" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-19&show-day class="daynum">19</a></h2></div>
			<div id="2020-02-20" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-20&show-day class="daynum">20</a></h2></div>
			<div id="2020-02-21" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-21&show-day class="daynum">21</a></h2></div>
			<div id="2020-02-22" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-22&show-day class="daynum">22</a></h2></div>
			<div id="2020-02-23" 
				class=" end   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-23&show-day class="daynum">23</a></h2></div>
			<div id="2020-02-24" 
				class=" start   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-24&show-day class="daynum">24</a></h2></div>
			<div id="2020-02-25" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-25&show-day class="daynum">25</a></h2></div>
			<div id="2020-02-26" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-26&show-day class="daynum">26</a></h2></div>
			<div id="2020-02-27" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-27&show-day class="daynum">27</a></h2></div>
			<div id="2020-02-28" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-28&show-day class="daynum">28</a></h2></div>
			<div id="2020-02-29" 
				class="   day">
				<h2 class="daynum"><a href=show.php?date=2020-02-29&show-day class="daynum">29</a></h2></div>
			<div id="" 
				class=" end   day">
				<h2 class="daynum"></h2></div></div>

		<div class='clear'></div>
	</div>
</div>	</div>
	<!-- tooltip handler -->
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
	</html>
	