<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $page_title; ?> | PHP Calendar</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  	
   	<!-- Custom styles for this template SIGN IN-->
    <link href="css/signin.css?v<?php echo time(); ?>" rel="stylesheet">
    
   	<!-- Custom styles for this template DASHBOARD-->
    <link href="css/dashboard.css?v<?php echo time(); ?>" rel="stylesheet">
    
    <!-- Calendar styles -->
    <link href="css/calendar.css?v<?php echo time(); ?>" rel="stylesheet">
    
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
	$(document).ready(function() {

		// -----------------------------------------------------------------------
		$.each($('#navbar').find('li'), function() {
			$(this).toggleClass('active', 
				window.location.pathname.indexOf($(this).find('a').attr('href')) > -1);
		}); 
		// -----------------------------------------------------------------------
		
	});
	</script>
	<!-- Basic style -->
	 <style>
		table.weekly th, table.weekly td{border-right: 1px solid #e0e0e0;border-bottom: 1px solid #e0e0e0; text-align:center;}
	</style>
</head>