<?php

// Google Auth
//require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');
require_once("db/db-init.php");			// db connection
require('calendar_head.php');					// basic functions

if (isset($_SESSION['login2app'])) {				// redirect back to index if already signed in
header("Location: http://" . $_SERVER['HTTP_HOST']
		   . dirname($_SERVER['PHP_SELF']) . '/'
		   . "index.php");
exit;
}

require_once 'PasswordLib.phar';		// pwd library, for login
$lib = new PasswordLib\PasswordLib();
	
$page_title = "Login ";						// set page title

include('calendar_header.php');					// html header

// password check
if (isset($_POST['username']) AND isset($_POST['passwd'])) {
	// basic user authentication (password)
	$username = $_POST['username'];
	$passwd = $_POST['passwd'];

	$sql = "SELECT username, password
		FROM cal_usrs 
		WHERE username = :username";
	$stmt = $db->prepare($sql);
	$stmt->execute(array($username));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);  
  
    if ($stmt->rowCount() == 1 AND $lib->verifyPasswordHash($passwd, $row['password'])) {
        $_SESSION['login2app'] = true;
        $_SESSION['username'] = $_POST['username'];
        header("Location: http://" . $_SERVER['HTTP_HOST']
                                   . dirname($_SERVER['PHP_SELF']) . '/'
                                   . "index.php");
        exit;
    } else {
        $_SESSION['fail'] = "Incorrect username/password!";
    }
}

// authentication via google
// requests authentication from user for the calendar
// finds users email and compares it users in db
//$client = new Google_Client();
//$client->setAuthConfig('/home/dnapaint/etc/calendar/client_secret.json');
//$client->addScope(Google_Service_Calendar::CALENDAR);
//$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	$client->setAccessToken($_SESSION['access_token']);
	$service = new Google_Service_Calendar($client);

	$user = new Google_Service_Plus($client);
	$person = $user->people->get('me');
	$user_email = ($person['emails'][0]['value']);
	
	// authentication test for google
	if($user_email != ''){
		$sql = "SELECT username
			FROM cal_usrs
			WHERE username = :username";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($user_email));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);  
	  
	    if ($stmt->rowCount() == 1) {
	        $_SESSION['login2app'] = true;
	        $_SESSION['username'] = $user_email;
	        header("Location: http://" . $_SERVER['HTTP_HOST']
	                                   . dirname($_SERVER['PHP_SELF']) . '/'
	                                   . "index.php");
	        exit;
	    } else {
	        $_SESSION['fail'] = "I pitty you fool!";
	    }
	}	
}

?>
<body>
<!-- main div container start -->
<div class="container" style="margin:10px auto;">
	<form class="form-signin" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<?php showMsg(); /* error messages */ ?>	
		<label for="inputUsername" class="sr-only">Username</label>
		<input type="text" name="username" id="inputUsername" class="form-control" placeholder="username" required>
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" name="passwd" id="inputPassword" class="form-control" placeholder="password" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
	</form>
	<?php
	showMsg();
	echo '<div class="ggl_sign_in"><a href="http://' . $_SERVER['HTTP_HOST'] . '/calendar/google-api-php-client-2.2.0/ggl/oauth2callback.php"><img height="50"  src="signin_button.png" /></a></div>';
	
	if($_SESSION['debug'] == 1){
		echo '<div><p align="center"><a href="calendar_logout.php" class="btn btn-default"><span class="glyphicon glyphicon-log-out"></span> Logout </a></p></div>';
	}?>
</div>
<!-- main div container end -->
</body>
</html>