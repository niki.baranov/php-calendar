<?php

// Google Auth
//require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');
session_start();

//$client = new Google_Client();
//$client->setAuthConfig('/home/dnapaint/etc/calendar/client_secret.json');
//$client->addScope(Google_Service_Calendar::CALENDAR);
//$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	$client->setAccessToken($_SESSION['access_token']);
	$service = new Google_Service_Calendar($client); 
}

// unset session
if (isset($_SESSION['login2app'])) {
    unset($_SESSION['login2app']);
    
    // unset api session var
    unset($_SESSION['access_token']);
    // to revoke token, call this (requires api lib)
   // $client->revokeToken();
}

// destroy session data
function destroy_session() {
   // reset the array
   $_SESSION = array();
   // destroy the cookie
   if (isset($_COOKIE[session_name()])) {
	  setcookie(session_name(), '', time()-86400, '/');
   }
   // destroy the session
   session_destroy();
}

destroy_session();

// open login page
header("Location: http://" . $_SERVER['HTTP_HOST']
                           . dirname($_SERVER['PHP_SELF']) . '/'
                           . "calendar_login.php");
?>