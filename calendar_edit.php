<?php
require_once('calendar_head.php');			// general functions
require_once("db/db-init.php");		// db connection

// check that user is logged in, even though already done in calendar_head.php
if($_SESSION['login2app'] != true){
	 $_SESSION['fail'] = "Noup, can't tuoch this.";
}
// admin or id match
else{
$ggl_api_call = NULL;
// get data from calendar_add_event.php:
$event_id	= (isset($_REQUEST['id']))		? $_REQUEST['id'] : '';
$event_id_ggl	= (isset($_REQUEST['event_id_ggl']))		? $_REQUEST['event_id_ggl'] : '';
$event_name	= (isset($_REQUEST['event_name']))	? $_REQUEST['event_name'] : '';
$contact	= (isset($_REQUEST['contact']))		? $_REQUEST['contact'] : '';
$start  	= (isset($_REQUEST['start']))		? $_REQUEST['start'] : '';
$end		= (isset($_REQUEST['end']))		? $_REQUEST['end'] : '';
$location	= (isset($_REQUEST['location']))	? $_REQUEST['location'] : '';
$type    	= (isset($_REQUEST['type']))		? $_REQUEST['type'] : '';
$size		= (isset($_REQUEST['size']))		? $_REQUEST['size'] : '';
$package	= (isset($_REQUEST['package']))		? $_REQUEST['package'] : '';
$deposit	= (isset($_REQUEST['deposit']))		? $_REQUEST['deposit'] : '';
$other_info	= (isset($_REQUEST['other_info']))	? $_REQUEST['other_info'] : '';
// process sql, use prepared statement
$q_event = <<<newEvent
UPDATE events
SET contact=:contact, start=:start, end=:end, location=:location, type=:type, size=:size, package=:package, deposit=:deposit, other_info=:other_info
WHERE event_id=:event_id
newEvent;

$new_event = $db->prepare($q_event);

// define parameters in the sql statement
$new_event->execute(array(':event_id'=>$event_id,
			  ':contact'=>$contact,
			  ':start'=>$start,
			  ':end'=>$end,
			  ':location'=>$location,
			  ':type'=>$type,
			  ':size'=>$size,
			  ':package'=>$package,
			  ':deposit'=>$deposit,
			  ':other_info'=>$other_info));

// google api auth check & call
	if (isset($_SESSION['access_token']) && $_SESSION['access_token'] && $event_id_ggl != '') {
		$client->setAccessToken($_SESSION['access_token']);
		$service = new Google_Service_Calendar($client);
		
		// get the event object from api based on $event_id_ggl
		$calendarId = 'primary';
		$event = $service->events->get($calendarId, $event_id_ggl);
		
		// set new data
		$event->setSummary('New title in google Calendar');
		
		$updatedEvent = $service->events->update($calendarId, $event->getId(), $event);
		
		// set api call variable
		$ggl_api_call = "ok";
	}
	
	else{
		$ggl_api_call = NULL;
	}
// google api call end

	if ($new_event->rowCount()!=0){
		if($ggl_api_call == "ok"){
			$_SESSION['success'] = "Event updated locally and in Google Calendar!";
		}
		else{
			$_SESSION['success'] = "Event updated locally!";
		}
	}
	else{
		$_SESSION['fail'] = "Sorry bru, Could not update this event!";
	}
}

header("Location: http://" . $_SERVER['HTTP_HOST']
		   . dirname($_SERVER['PHP_SELF']) . '/'
		   . "index.php");
?>