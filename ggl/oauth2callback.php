<?php
require_once ('/home/dnapaint/etc/calendar/vendor/autoload.php');

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('/home/dnapaint/etc/calendar/client_secret.json');
$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/calendar/google-api-php-client-2.2.0/ggl/oauth2callback.php');
$client->setAccessType("offline");
$client->addScope(Google_Service_Calendar::CALENDAR);
$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);

if (! isset($_GET['code'])) {
  $auth_url = $client->createAuthUrl();
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
}
else {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/calendar/google-api-php-client-2.2.0/calendar_login.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}