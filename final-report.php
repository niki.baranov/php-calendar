<?php
// start session
session_start();
// session login check, if not true or not set redirect to login page
if ((!isset($_SESSION['login2app']) || $_SESSION['login2app'] !== true) && basename($_SERVER['REQUEST_URI'])!='calendar_login.php') {
header("Location: http://" . $_SERVER['HTTP_HOST']
						   . dirname($_SERVER['PHP_SELF']) . '/'
						   . "calendar_login.php");
exit;

}
$page_title = "PHP Calendar Documentation";		// set page title
include('calendar_header.php');		// html header

?>
<body style="margin:0px; height:100%; width:100%">
<?php
include('calendar_menu.php');
?>
<div class='container' style='margin:30px auto;'>
<h1>The final report</h1>
<h3>Documentation menu</h3>
<a href="plan1.php" >Preliminary plan</a><br>
<a href="final-plan.php" >Final plan</a><br>
<a href="final-report.php" >Final report</a><br>
<a href="time.php" >Time keeping / schedule</a><br>
<p></p>
<a href="php-syntax-highlighter.php" >Syntax highlighter Main</a><br>
<a href="ggl/php-syntax-highlighter.php" >Syntax highlighter GGL</a><br>
<a href="calendar.zip" >Project ZIP (without Google API Library)</a><br>
<a href="https://github.com/google/google-api-php-client/releases" > Google API Library</a><br>
<p></p>
<h2>About the app and code</h2>
<p>Apart from Google API and a calendar.php class file that is used for monthly view, all of the code was written specifically for this project. I did try to comment methods and &lsquo;what is happening&rsquo; and why as well as I could to allow for easy conversion of the app for needs of other possible users.<br>
  This calendar app is a PHP application that features Google API for authentication and synchronization if user wants, this functionality can be easily disabled by changing XXX in XXX.</p>
<h2>Technical details</h2>
<p>This application is PHP based and uses MySQL database as storage of event and minor user data. Application was created and tested on PHP 7.0.25 and MySQL 5.5.32 running on Apache 2.0 server.<br>
  Compatibility with other versions of PHP, MySQL and Apache have not been tested, however I doubt there are any issues as the code relies mostly on PHP date-object.<br>
  To setup Google API authentication and Calendar sync, installation of Google API library is necessary as well as creation of API key for your application via Google Developer console.</p>
<h3>Event management</h3>
<p>Calendar_show_event.php, calendar_add_event.php, calendar_add.php, calendar_edit_event.php, calendar_edit.php, calendar_remove.php</p>
<h3>Views</h3>
<p>Index.php renders all main views based on date and parameters passed through url.<br>
  showDay – renders entries for current or selected day, placing each entry in its hour slot.<br>
  showWeek – renders weekly calendar that shows quick links for each event<br>
  showEvent – shows details of the selected event<br>
  Entries are color coded to allow for easy recognition of the location where each event is supposed to be at.<br>
  Monthly view is rendered via calendar.php file which renders the full month and populates each day with found events maintaining the same color codes. This view is based on third-party php script that was slightly modified to feature missing functionality.</p>
<h3>Login &amp; authentication</h3>
<p>Calendar_login.php, calendar_logout.php, oauth2callback.php (google API)<br>
  Login is possible by using username/password as well as using Google API for authentication of the user. If authentication is done locally, database is checked for password and username combo, upon which a session based variable is created to authenticate user. Authentication check is done on every page, even the hardcopy, to prevent unauthorized access to calendar data.<br>
  When authentication Is done via Google account, Google API requests calendar access from the user. Upon allowed access Google API creates an authentication session variable for the current session. User email is also retrieved from Google account as well as local DB and their match is verified. Upon verification, a session based authentication is created and user is logged in into the system.<br>
  At the moment system does not allow for extra users, mainly because such functionality was not needed. A source file for user creation is however provided (add_new_user.php).</p>
<h3>Event synchronization</h3>
<p>Calendar_sync.php (Google API)<br>
  Event sync is automated but is also available as a manual function. Upon update of any event, system sends updated information to Google Calendar via Google API as well as writes update to the local database. Synchronization is only available/possible if user has logged in with Google account or has allowed Google Calendar synchronization upon its request.</p>
<p>Setting up Google API will require downloading the latest version of Google API library. In this project i used<strong> google-api-php-client-2.2.0</strong> which can be dowloaded from <a href="https://github.com/google/google-api-php-client/releases">google</a>. In addition to this, you have to setup  your API project here : https://console.developers.google.com. As this was only a small portion of the project that did not require much fiddling, i wont go through API setup.</p>
<h3>Creation of non-dynamic files (hard-copy)</h3>
<p>Calendar_create_hard_copy.php<br>
  To minimize DB-calls and to speed up load times I decided to create a hard-copy of most used files. These files still require authentication for access, but otherwise do not require DB-calls.</p>
<p>Hard copy is only available for current month/weekly/day views as they are likely to be opened the most. Ideally each file is automatically created upon modification of an event (add, edit, delete) or on calendar sync, in addition, sync could be setup via cron,  however neither was automated   for this version.</p>
<h3>Other scripts</h3>
<p>Calendar_menu.php, calendar_head.php, calendar_header.php, db-init.php<br>
  These files are responsible for database connection, menu and header rendering.</p>
<h3>Task &amp; schedule</h3>
<p>Detailed schedule can be seen below. The initial estimation of the project was around 50 hours, the final tally was XX mainly due to fiddling with UI and UX as well as reconfiguring system for OOP. Initially I wanted to complete the project by the end of week 47. This means a 3-day extension which was due to a rather slow start for the initial coding process.  as well as some issues with API and conversion of view files to showCalendar.class. </p>
<p>I spend much more time on this than initially thought would be necessary, toward the end i had to cut some corners which meant leaving out some good to have functionality like administration and sync scheduling.</p>
<p><span class="container" style="margin:30px auto;"><a href="http://dna-paintball.co.za/calendar/google-api-php-client-2.2.0/docu/time.php" >Time keeping / schedule</a></span></p>
<h3>Project evaluation</h3>
<p>Google API has is currently used on vast number of websites so getting to know how it works was very interesting, it was also extremely interesting to see how big companies publish their code and document it. I was extremely satisfied that API system was relatively easy to use and required  minimal &lsquo;head-scratching&rsquo; although at the same time disappointed with lack of easy to use/setup examples.</p>
<p>One of the main reasons for this project was to get to better understanding about PHP date object. I was positively surprised that I was able to create all necessary functionality that I initially planned for without relying too much on a ready library or class.<br>
  After working with available methods, I am quite sure that I would be able to create a similar monthly view generating class as I used in this project. For now, I will keep the found class.</p>
<p>The scope of this  project seemed quite minute at first but as the deadline grew closer it seemed to grow quite a bit. As a whole, I am very happy for how this project turned out and hope my friend will be able to utilize it in his work.</p>
<p>I was also not able to find a similar open source project so I hope others will find this application useful once I get it finalized.</p>
<p>It&rsquo;s seems extremely likely that I will continue to work on this project and possibly convert it into a smartphone app. Further updates to the system could allow management of what events are synced, how often items are synced as well as addition and management of users and possibly some other administration tools.</p>
<h3>Self-evaluation</h3>
<p>Considering that I was doing this alone and that all planned functions were completed in addition to spent time, I think I deserve 5/5 for it even though there are some parts that could have been done in a more sophisticated way. Overall im quite happy with the result and learned quite a bit about while doing this project.</p>
<h2>Files in (some more) detail</h2>
<h3>Index.php, this-week.php and today.php</h3>
<p>These files are &lsquo;hard&rsquo;-copy generated to minimize unnecessary DB calls. Files are generated through calendar_create_hard_copy.php with a passed url variable &lsquo;show-day&rsquo; and &lsquo;show-week&rsquo; to create a copy of todays and this week&rsquo;s view (generated by show.php). If no extra variables are passed, creates a monthly version of current month.</p>
<h3>Calendar.php</h3>
<p>Monthly calendar is shown by calling show() method from index.php after initiating calendar object. <br>
  This class is not using any external libraries and only utilizes PHP native calendar functionality (date ,datetime etc.)<br>
  Main show() method introduces year and month based on current date and calls other methods to compile a single content object that is returned. _ShowDay method is responsible for populating correct days with data and positioning them according to weekday.<br>
  Slight modification to the original file was made to allow for populating correct days with possible events and making days and events clickable.</p>
<h3>showCalendar.class.php</h3>
<p>This is a  class that is used to generate weekly, daily and event specific views when called. It uses some similar methods as calendar.php. Main methods are showDay, showWeek and showEvent, other, smaller methods are used to create navigation and make database calls.</p>
<h3>Calendar_head.php</h3>
<p>This is the file that authenticates user after authentication is completed on the login page.</p>
<h3>Db-init.php</h3>
<p>This file is located out of reach for non-local access and is mainly used by private functions of calendar.php and showCalendar.php. Other files that have need for DB connection included it directly. </p>
<h3>Calendar_login.php</h3>
<p>Handles the authentication of the user, unless user has been authenticated, no other pages are available. In addition to having database access, loads PasswordLib for password authentication. Also contains a simple login form that passes its data via POST to itself, upon this simple IF check tries to authenticate passed values.<br>
  Has a connection to Google API to handle user authentication with Google account, upon allowed access by the user, checks for users email and verifies its existence from DB.</p>
<h3>Calendar_logout.php</h3>
<p>Destroys the current session and removes possible related cookie. Redirects user to login page after session is destroyed. Contains very simple function.<br>
  Also destroys Google authentication session.</p>
<h3>Calendar_menu.php</h3>
<p>Contains the navigation menu for the site. Checks for google authentication session variable to show active/inactive session. Features a link to sync current calendar with Google Calendar.</p>
<h3>Calendar_header.php</h3>
<p>Contains main html header elements such as metatags, title, stylesheet links, the only file that does not require calendar_head.php file. Prints $page_title.</p>
<h3>Calendar_add_event.php</h3>
<p>Form required to add new event to the database, passes form data via POST to calendar_add.php file, loads calendar_head.php, calendar_header.php and calendar_menu.php. Has no other functionality.</p>
<h3>Calendar_add.php</h3>
<p>Uses data passed by the form at calendar_add_event.php to add new event into database. Loads calendar_head.php but no other files as it does not return visible data to the user, redirects to index.php upon completion.<br>
  Collects form data from request. Prepares sql statement and binds passed form data to variables.<br>
  Also adds event data to Google Calendar via API.<br>
  Due to the difference between local and Google Calendar event data, most of the information is passed to the &lsquo;description&rsquo; field of Google Calendar event.</p>
<h3>Calendar_remove.php</h3>
<p>Removes selected event from DB as well as Google Calendar if event has the correct event_id_ggl.</p>
<h3>Show.php</h3>
<p>Shows different views based on passed url. <br>
  Show.php?show-day, shows current day, passing date=YYYY-MM-DD shows that day&rsquo;s events. <br>
  Show.php?show-day, shows current day, passing date=YYYY-MM-DD shows that week&rsquo;s events. <br>
  Show.php, default view is current month, passing date=YYYY-MM-DD shows that months events. <br>
  Uses class calender.php and showCalendar.class.php for functionality.</p>
<h3>Calendar_sync.php</h3>
<p>Connects to Google API to fetch the hardcodeds amount of entries starting from the current date.<br>
  Checks whether event is already in the db, if not creates new, if yes, updates old.</p>
Collects data via REGEXP to populate fields in database. 
</div>
<!-- tooltip handler -->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
<!-- Latest compiled and minified JavaScript -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
