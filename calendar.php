<?php
/**
*Original:
*@author  Xu Ding
*@email   thedilab@gmail.com
*@website http://www.StarTutorial.com
*
*Modified version for a non-profit school project
*@author  Niki Baranov
*@email   niki.baranov@gmail.com
*
*added functionality:
*gets events from db via getEvents
*adds link to an event
*other minor, mainly visual changes
*
**/

class Calendar {  
     
    /**
     * Constructor
     */
    public function __construct(){     
        //$this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->naviHref = "show.php";
    }
     
    /********************* PROPERTY ********************/  
    private $dayLabels = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
    private $currentYear=0;
    private $currentMonth=0;
    private $currentDay=0;
    private $currentDate=null;
    private $daysInMonth=0;
    private $naviHref= null;
    /********************* PUBLIC **********************/  
        
    /**
    * print out the calendar
    */
    public function show() {
	// db connection
	require("db/db-init.php");
    
	$events = $this->getEvents($db);
        $year  = null;
        $month = null;
        $found_events = array();
        
        // create a simple array from received pdo object
        foreach($events as $event){
		$found_events[] = array("event_date" => date_format(date_create($event['start']),"Y-m-d"),
				  "event_name" => $event['event_name'],
				  "contact" => $event['contact'],
				  "location" => $event['location'],
				  "event_id" => $event['event_id']);
	}
	
        if(null==$year&&isset($_GET['year'])){
            $year = $_GET['year'];
        }else if(null==$year){
            $year = date("Y",time());  
        }  
		
        if(null==$month&&isset($_GET['month'])){
            $month = $_GET['month'];
        }else if(null==$month){
            $month = date("m",time());
        }               
         
        $this->currentYear=$year;
        $this->currentMonth=$month;
        $this->daysInMonth=$this->_daysInMonth($month,$year);  
        $content='<div id="calendar">'.
                        '<div class="box">'.
                        $this->_createNavi().
                        '</div>'.
                        '<div class="box-content">'.
                                '<div class="label">'.$this->_createLabels().'</div>';   
                                $content.='<div class="clear"></div>';     
                                $content.="\n\t\t<div class='dates'>"; 
                                $weeksInMonth = $this->_weeksInMonth($month,$year);
                                // Create weeks in a month
                                
                                for( $i=0; $i<$weeksInMonth; $i++ ){
                                    //Create days in a week
                                    for($j=1;$j<=7;$j++){
                                        $content.=$this->_showDay($i*7+$j,$found_events);
                                    }
                                }
                                $content.="</div>\n";
                                $content.="\n\t\t<div class='clear'></div>";     
                        $content.="\n\t</div>";
        $content.="\n</div>";
        return $content;   
    }
	
    /********************* PRIVATE **********************/ 
    /**
    * create the li element for ul
    * modified to use divs instead, also adds today check and links to each event
    */
    private function _showDay($cellNumber,$events){
	$today = date("Y-m-d");
	$event_id = null;
	$same_day = 0;
	$found_events = '';
	
        if($this->currentDay==0){
            $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
			$date = new DateTime($this->currentYear.'-'.$this->currentMonth.'-01');
            if(intval($cellNumber) == intval($firstDayOfTheWeek)){
                $this->currentDay=1;
            }
		$week = $date->format("W");
        }
        if( ($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth) ){
            $this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
            $cellContent = $this->currentDay;
            $this->currentDay++;
			
			$date = new DateTime($this->currentDate);
			$week = $date->format("W");
        }else{	
			$week=null;
            $this->currentDate =null;
            $cellContent=null;
        }
	
	foreach($events as $event){
		if($event['event_date']==$this->currentDate){
			if ($found_events != ''){
				$found_events .= '<a href="'.$this->naviHref.'?id='.$event['event_id'].'" class="event-'.$event['location'].'">
						<span class="event">'.($event['contact']!=''?$event['contact']:$event['event_name']).'</span></a>';
			}
			else{
				$found_events = '<a href="'.$this->naviHref.'?id='.$event['event_id'].'" class="event-'.$event['location'].'">
						<span class="event">'.($event['contact']!=''?$event['contact']:$event['event_name']).'</span></a>';
			}
		}
	}
	
	$is_link = '<a href='.$this->naviHref.
			'?date='.$this->currentYear.
			'-'.$this->currentMonth.
			'-'.$cellContent.
			'&show-day class="daynum">'.
			$cellContent.'</a>';
	
	return "\n\t\t\t".'<div id="'.$this->currentDate.'" 
				class="'.
				($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
				($this->currentDate==$today?' today ':' ').
				' day">
				<h2 class="daynum'.($this->currentDate==$today?'-today':'').'">'.
				($cellContent!=''?$is_link:'').
				'</h2>'.$found_events.'</div>';
    }
	
    /**
    * create navigation
    */
    private function _createNavi(){
        $nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
        $nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
        return
            '<div class="header">'.
                '<a class="prev" href="'.$this->naviHref.'?month='.sprintf('%02d',$preMonth).'&year='.$preYear.'"><span class="glyphicon glyphicon-step-backward"></span> Prev</a> '.
                    '<span class="title">'.date('F Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span> '.
                '<a class="next" href="'.$this->naviHref.'?month='.sprintf("%02d", $nextMonth).'&year='.$nextYear.'">Next <span class="glyphicon glyphicon-step-forward"></span></a>'.
            '</div>';
    }
         
    /**
    * create calendar week labels
    */
    private function _createLabels(){  
        $content='';
        foreach($this->dayLabels as $index=>$label){
            $content.='<div class="'.($label==6?'end title':'start title').' title">'.$label.'</div>';
        }
        return $content;
    }   
     
    /**
    * calculate number of weeks in a particular month
    */
    private function _weeksInMonth($month=null,$year=null){
        if( null==($year) ) {
            $year =  date("Y",time()); 
        }
         
        if(null==($month)) {
            $month = date("m",time());
        }
         
        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month,$year);
        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
        $monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));
        $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));
        if($monthEndingDay<$monthStartDay){
            $numOfweeks++;
        }
        return $numOfweeks;
    }
 
    /**
    * calculate number of days in a particular month
    */
    private function _daysInMonth($month=null,$year=null){
        if(null==($year))
            $year =  date("Y",time()); 
        if(null==($month))
            $month = date("m",time());
        return date('t',strtotime($year.'-'.$month.'-01'));
    }
    
    /**
    * getEvents
    * get events for the current month
    */
private function getEvents($db){
	if(isset($_GET['year'])&&isset($_GET['month'])){
		$date = $_GET['year']."-".$_GET['month']."-01";
	}
	else{
		$date = date("Y-m-d");
	}
	// process sql, use prepared statement
	$q_events = <<<getEvents
	SELECT * 
	FROM events 
	WHERE DATE_FORMAT(start, '%Y-%m')=DATE_FORMAT(:month, '%Y-%m')
	ORDER BY start
getEvents;
		$events = $db->prepare("$q_events");
		// define parameters in the sql statement
		$events->execute(array(':month'=>$date));
	   return $events;
	}
}